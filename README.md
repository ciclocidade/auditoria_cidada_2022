## Auditoria Cidadã 2022 - Conversão para extensão CET

![Mapa Auditoria Cidadã 2022](/images/auditoria-cidada-2022-mapa.png "Auditoria Cidadã 2022")

Publicada em junho de 2022, a **Auditoria Cidadã 2022** da rede cicloviária de São Paulo apontou que 81% da malha cicloviária atual encontrava-se em boas condições, enquanto outros 19% exigiam atenção. 

O cálculo foi feito de forma proporcional devido à forma como a CET registra as quilometragens oficiais na base de dados, removendo as extensões "duplicadas" de estruturas unidirecionais situadas aos dois lados do viário, em especial em vias separadas por canteiro central. Isso faz com que as extensões sejam diferentes das calculadas pelo QGIS/R, que não removem tais "duplicações".

Neste repositório encontram-se o arquivo *shapefile* final do levantamento de campo, com todas as avaliações, um arquivo de QGIS com os filtros de visualização por item de manutenção e um script em R para calcular as extensões que requerem manutenção tendo como base o cálculo da CET.

## Dados gerais

### Resultados Auditoria Cidadã 2022
Nesta tabela, as extensões por estado geral de manutenção são as calculadas pelo QGIS (extensão da linha, em metros, para uma projeção em SIRGAS 2000 23S). O relatório da Auditoria Cidadã e o mapa interativo online (ver links abaixo) têm como base estes valores.

|Manutenção geral|Extensão QGIS (m)|Proporcional|
|------------|------------------:|-----------:|
|Boa         | 694.127           |    80,8%   |
|Razoável    | 102.947           |    12,0%   |
|Precária    | 47.104            |     5,5%   |
|Inexistente | 15.043            |     1,8%   |
|**Total**   | **859.221**       | **100,0%** |

<br>

### Extensões oficiais CET (base original)
Nesta tabela estão as somas oficiais da CET. Pela quilometragem oficial, foram avaliados quase 667 kms de estrutura cicloviária da cidade.

|Extensões CET|Ext. (m)|
|---------------------------|------------:|
|Extensão CET (soma)        |   860.091   |
|Extensão CET (menos)       |   193.257   |
|**Extensão CET (oficial)** | **666.834** |

<br>

### Resultados Auditoria Cidadã 2022 com extensões oficiais CET

Nesta tabela, os resultados das duas primeiras linhas levam em consideração o estado geral de manutenção considerado **bom** versus os demais estados (razoável, precário e inexistente). Há casos em que o estado geral foi considerado bom, mas o estado do pavimento foi avaliado como razoável - as somas e proporções ao considerar também este elemento são apresentadas nas linhas 3 e 4 da tabela. Nas linhas finais, o cálculo foi repetido considerando combinações diferentes entre estados de manutenção e pavimento.

| Estado de manutenção | Ext. QGIS (m) | Prop. QGIS | Ext. CET (m) | Prop. CET |
|-----------------------------------------------|--------:|------:|----------:|------:|
|Manutenção: Boa                                | 694.127 | 80,8% | 523.001,4 | 78,4% |
|Manutenção: Outros (Razoável, Precário, Inex.) | 165.093 | 19,2% | 143.832,6 | 21,6% |
|Manutenção: Boa + Pavimento: Bom               | 675.511 | 78,6% | 507.071,0 | 76,0% |
|Manutenção: Outros + Pavimento: Outros         | 183.710 | 21,4% | 159.763,0 | 24,0% |
|Manutenção: Boa/Razoável + Pavimento: Bom      | 737.137 | 85,8% | 560.767,2 | 84,1% |
|Manutenção: Precário/Inex. + Pavimento: Outros | 122.084 | 14,2% | 106.066,8 | 15,9% |

<br>

## Links de referência


![Capa Auditoria Cidadã 2022](/images/auditoria-cidada-2022.jpg "Capa Cidadã 2022")

O relatório completo da Auditoria Cidadã 2022 pode ser acessado neste link: https://www.ciclocidade.org.br/wp-content/uploads/2022/07/Auditoria-Cidada-Estrutura-Cicloviaria-SP-2022.pdf

O mapa interativo online, com as 7.800 fotos georreferenciadas, pode ser acessado neste link: http://mobilidadeativa.org.br/auditoria-cidada-2022/mapa/
